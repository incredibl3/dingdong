Puralax game

```
#!javascript

Puralax Game is a simple puzzle game where you need to paint every colored tile into one single color to complete each level. Each level has a different target color, indicated by the color of the top bar on the screen. 

```

Project Setup
Requirement
Devkit

Install Project

```
#!javascript

git clone https://bitbucket.org/incredibl3/puralax.git
```


At your project dir

```
#!javascript

devkit install
```

After install, please replace effectsLibrary.js file src folder to {project_path}/modules/devkit-effects/src (I modified some code :P)


Run Project

```
#!javascript

devkit serve
```


Open browser and visit: http://localhost:9200/