import device;
import animate;
import AudioManager;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import ui.widget.GridView as GridView;

import src.Config as config;
import math.util as util;
import math.geom.Line as Line;
import effects;
import src.lib.uiInflater as uiInflater;

import entities.Entity as Entity;
import entities.EntityPool as EntityPool;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

var app;
var SHOW_HIT_BOUNDS = false;

rollFloat = function(min, max) {
  return min + Math.random() * (max - min);
};

//## Class: Application
exports = Class(GC.Application, function() {
  this._settings = {
    alwaysRepaint: true,
    logsEnabled: false,
    showFPS: false,
    preload: ["resources/images"]
  };
  /**
   * initUI
   * ~ called automatically by devkit
   * ~ initialize view hierarchy and game elements
  */
  this.initUI = function() {
    app = this;

    this._time = 0;

    //scale screen
    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        background: {
          volume: 0.8,
		  background: true,
          loop: true
        },
        touch: {
          volume: 0.5,
          loop: false
        },
        break: {
          volume: 0.5,
          loop: false
        },
        addscore: {
          volume: 0.5,
          loop: false
        },
        clock: {
          volume: 1,
          loop: true
        },
        super: {
          volume: 0.5,
          loop: true
        }
      }
    });

    //grid background
    this.gridViewLayer  = new GridView({
      superview: this.view,
      layout: 'box',
      x: -299.5, // (576 - 1175) / 2; 1175 = length of diagonal line
      y: -75.5, // (1024 - 1175) / 2; 1175 = length of diagonal line
      width: 1175,
      height: 1175,
      cols: config.BG_CELL,
      rows: config.BG_CELL,
      opacity: 0.8,
      zIndex: 1,
      anchorX: 587.5,
      anchorY: 587.5,
      autoCellSize: true,
      //Make hide cells out of the grid range...
      hideOutOfRange: true,
      //Make cells in the grid range visible...
      showInRange: true
    });
    this.gridViewLayer.updateOpts({verticalGutter: 2, horizontalGutter: 2});

    for (var y = 0; y < config.BG_CELL; y++) {
      for (var x = 0; x < config.BG_CELL; x++) {
        new View({superview: this.gridViewLayer, backgroundColor: "rgb(20, 20, 40)", col: x, row: y});
      }
    }
    //end grid background

	new ImageView({
      parent: this.view,
      layout: 'box',
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      zIndex: 3,
      image: "resources/images/border_background.png"
    });

    animateBackground();

    this.showPlayScreen();

    this.isGameOver = false;
    this.bestscore = 0;
    this.isRestartScreen = false;

    //hungnt remove clock power view when hit obstacles
    this.clockPowerImgView = [];
  };

  this.showPlayScreen = function(){
    //Play screen
    this.title = new TextView({
      superview: this.view,
      layout: "box",
      text: "TING",
      color: "rgb(130, 198, 40)",
      wrap: true,
      autoSize: true,
      autoFontSize: true,
      centerX: true,
      y: 80,
      width: BG_WIDTH,
      height: 100,
      size: 100,
      zIndex: 3,
      fontFamily: "Arial",
      fontWeight: 'bold',
      shadowColor: "white",
      shadowWidth: 3,
      clip: true
    });

    this.playBtn = new ImageView({
      parent: this.view,
      layout: 'box',
      centerX: true,
      y: 500,
      width: 408,
      height: 158,
      zIndex: 3,
      image: "resources/images/play.png"
    });
    //end play screen

    this.playBtn.on("InputSelect", function(event, point){
      app._sound.play("background", {loop: true});
      app.playGame();
      app.title.removeFromSuperview();
      app.playBtn.removeFromSuperview();
      app.isPlayScreen = true;
    });
  }

  this.showEndScreen = function(){
    if(this.bestscore < this.score)
      this.bestscore = this.score;
    this.bestScoreTextView = new TextView({
      superview: this.view,
      layout: "box",
      text: "BEST \n " + this.bestscore,
      color: "white",
      wrap: true,
      autoSize: true,
      autoFontSize: true,
      centerX: true,
      centerY: true,
      width: 400,
      height: 200,
      size: 70,
      zIndex: 3,
      fontFamily: "Arial",
      clip: true
    });

    this.restartBtn = new ImageView({
      parent: this.view,
      layout: 'box',
      centerX: true,
      y: 700,
      width: 408,
      height: 158,
      zIndex: 3,
      image: "resources/images/restart.png"
    });

    this.restartBtn.onInputSelect = function(){
      app._sound.play("background");
      app.bestScoreTextView.removeFromSuperview();
      app.restartBtn.removeFromSuperview();
      app.isRestartScreen = true;
      app.reset();
    };
  }

  this.playGame = function(){
    //border view
    this.borderView = new View(merge({parent: this, x: 0, y: 0}, config.BorderView));
    uiInflater.addChildren(config.BorderView.children, this.borderView);

    this.borderColor = {r: 188, g: 52, b: 70};
    //end border view

    //score view
    this.score = 0;
    this.scoreTextView = new TextView({
      superview: this.view,
      layout: "box",
      text: "",
      color: "#FFFFFF",
      wrap: true,
      autoSize: true,
      autoFontSize: true,
      x: (BG_WIDTH * 3 / 4) - 50,
      y: 20,
      width: 170,
      height: 80,
      size: 70,
      zIndex: 3,
      fontFamily: "Arial",
      clip: true
    });

    //Guide text
    this.guideText = new TextView({
      superview: this.view,
      layout: "box",
      text: "Tap to move. \nAvoid obstacles. \nKeep moving!",
      color: "#FFFFFF",
      wrap: true,
      autoSize: true,
      autoFontSize: true,
      centerX: true,
      y: 150,
      width: 576,
      height: 100,
      size: 50,
      zIndex: 3,
      fontFamily: "Arial",
      clip: true
    });

    animate(this.guideText).now({opacity: 0}, 5000, animate.linear);
    //end guide text

    //Init Entities
    this.powers = new Powers({ parent: this.view });
    this.obstacles = new Obstacles({ parent: this.view });
    this.playerEntityPool = new PlayerEntityPool({ parent: this.view });

    //Handle touch event
    this.view.onInputSelect = function(){
      if(this.isPlayScreen){
        this.isPlayScreen = false;
        return;
      }
      if(this.isRestartScreen){
        this.isRestartScreen = false;
        return;
      }
      app.playerEntityPool.onInputSelect();
    };

    this.reset();
  }


  //gridview background animation
  animateBackground = function(){

    startColor = {r:255, g:  128, b:  128};  // red
    endColor   = {r:  0, g:128, b:128};  // dark turquoise

    app.fade(app.gridViewLayer, endColor, startColor,10000);
    animate(app.gridViewLayer).now({scale: 1.3}, 10000, animate.linear);

    setTimeout(function(){
      app.fade(app.gridViewLayer, startColor, endColor,10000);
      animate(app.gridViewLayer).now({scale: 1}, 10000, animate.linear);
    }, 10000);

    setTimeout(function(){
      animateBackground();
    }, 20000);
  }

  //choose random value in array
  choice = function (arr) {
    return arr[arr.length * Math.random() | 0];
  }

  //animate background color
  this.lerp = function(a,b,u) {
        return (1-u) * a + u * b;
  };

  this.fade = function(view, start, end, duration) {
    var interval = 100;
    var steps = duration/interval;
    var step_u = 1.0/steps;
    var u = 0.0;
    var theInterval = setInterval(function(){
      if (u >= 1.0){ clearInterval(theInterval) }
      var r = parseInt(app.lerp(start.r, end.r, u));
      var g = parseInt(app.lerp(start.g, end.g, u));
      var b = parseInt(app.lerp(start.b, end.b, u));
      var colorname = 'rgba('+r+','+g+','+b+', 0.5)';
      if(Array.isArray(view))
        view.forEach(function(child){
          child.updateOpts({backgroundColor: colorname});
        });
      else
        view.updateOpts({backgroundColor: colorname});
      u += step_u;
    }, interval);
  };
  //end animate background color

  /**
   * launchUI
   * ~ called automatically by devkit when its engine is ready
   */
  this.launchUI = function () {};

  /**
   * reset
   * ~ resets all game elements for a new game
   */
  this.reset = function(data) {
    this.score = 0;
    this.scoreTextView.setText(""+ this.score);
    this.isGameOver = false;
    this.powers.reset();
    this.obstacles.reset();
    this.playerEntityPool.reset();
  };

  /**
   * tick
   * ~ called automatically by devkit for each frame
   * ~ updates all game elements by delta time, dt
   */

  this.tick = function(dt) {

    //background rotation
    this._time += dt;
    this.gridViewLayer.style.r = this._time / 100000;

    // limit to 60 FPS per second
    dt = 16;

    //update entities
    if(this.playerEntityPool && this.isGameOver == false)
    {
      this.powers.update(dt);
      this.obstacles.update(dt);
      this.playerEntityPool.update(dt);

      this.playerEntityPool.onFirstPoolCollisions(this.obstacles, this.onObstacleHit, this);
      this.playerEntityPool.onFirstPoolCollisions(this.powers, this.onPowerHit, this);
    }
  };

  this.onObstacleHit = function(player, obstacle){
    console.log("player hit obstacle");

    if(app.playerEntityPool.isGodMode)
    {
      obstacle.shade1.removeFromSuperview();
      obstacle.shade2.removeFromSuperview();
      obstacle.destroy();
      app.obstacles.spawnCount--;
      app._sound.play("break");
      effects.firework(obstacle.view, {
        duration: 3000,
        scale: 0.9,
        images:
        [
          'resources/images/red_explode.png',
          'resources/images/green_explode.png',
          'resources/images/pink_explode.png',
          'resources/images/purple_explode.png',
          'resources/images/yellow_explode.png'
        ]
      });
    }else{
      this.playerEntityPool._trail.removeFromSuperview();
      player.destroy();
      animate(this.playerEntityPool.entities[0]).clear();

      for(var i = 0; i < app.obstacles.spawnCount; i++)
      {
        app.obstacles.entities[0].shade1.removeFromSuperview();
        app.obstacles.entities[0].shade2.removeFromSuperview();
        app.obstacles.entities[0].destroy();
      }
      app.obstacles.spawnCount = 0;

      effects.firework(player, {
        duration: 3000,
        scale: 0.9,
        images:
        [
          'resources/images/red_explode.png',
          'resources/images/blue_explode.png',
          'resources/images/green_explode.png',
          'resources/images/orange_explode.png',
          'resources/images/pink_explode.png',
          'resources/images/purple_explode.png',
          'resources/images/white_explode.png',
          'resources/images/yellow_explode.png'
        ]
      });
      app._sound.play("break");
      app._sound.stop("background");
      this.isGameOver = true;

      //hungnt remove clock power view when hit obstacles
      if(app.isClockPowerTicking == true){
      	for (var i = 0; i < 10; i++) {
      		app.clockPowerImgView[i].removeFromSuperview();
      	}
      	app._sound.stop("clock");
      	app.isClockPowerTicking = false;
      }
      this.showEndScreen();
    }
  }

  this.onPowerHit = function(player, power){
    console.log("player hit power");

    power.destroy();
    app.powers.spawnCount--;

    if(power.id == "addScore")
    {
      app._sound.play("addscore");
      this.score += 5;
      this.scoreTextView.setText("" + this.score);
    }else if(power.id == "bomb"){
      app._sound.play("break");
      for(var i = 0; i < app.obstacles.spawnCount; i++)
      {
        app.obstacles.entities[0].shade1.removeFromSuperview();
        app.obstacles.entities[0].shade2.removeFromSuperview();

        if(app.obstacles.entities[0].view._opts.image.indexOf("red_square") > -1)
        {
          effects.firework(app.obstacles.entities[0].view, {
            duration: 3000,
            images: ['resources/images/red_explode.png']
          });
        }else if(app.obstacles.entities[0].view._opts.image.indexOf("yellow_rectangle") > -1){
          effects.firework(app.obstacles.entities[0].view, {
            duration: 3000,
            images: ['resources/images/yellow_explode.png']
          });
        }else if(app.obstacles.entities[0].view._opts.image.indexOf("purple_rectangle") > -1){
          effects.firework(app.obstacles.entities[0].view, {
            duration: 3000,
            images: ['resources/images/purple_explode.png']
          });
        }else if(app.obstacles.entities[0].view._opts.image.indexOf("triangle") > -1){
          effects.firework(app.obstacles.entities[0].view, {
            duration: 3000,
            images: ['resources/images/green_explode.png']
          });
        }else if(app.obstacles.entities[0].view._opts.image.indexOf("polygon") > -1){
          effects.firework(app.obstacles.entities[0].view, {
            duration: 3000,
            images: ['resources/images/pink_explode.png']
          });
        }else if(app.obstacles.entities[0].view._opts.image.indexOf("star") > -1){
          effects.firework(app.obstacles.entities[0].view, {
            duration: 3000,
            images: ['resources/images/yellow_explode.png']
          });
        }

        app.obstacles.entities[0].destroy();
      }
      app.obstacles.spawnCount = 0;
    }else if(power.id == "clock"){
      app._sound.play("clock");
      app.isClockPowerTicking = true;
      for (var i = 0; i < 10; i++) {
        app.clockPowerImgView[i] = new ImageView({
          parent: app.view,
          x: util.random(150, 350),
          y: util.random(300, 600),
          width: 80,
          height: 80,
          zIndex: 3,
          opacity: 0,
          image: "resources/images/clock_notrail.png"
        });
        var fade_time = util.random(0, 2000);
        animate(app.clockPowerImgView[i]).clear().wait(fade_time)
        .then({opacity: 0.7}, 100, animate.linear)
        .then({x: util.random(0, BG_WIDTH), y: util.random(0, BG_HEIGHT), scale: util.random(1.5, 3)}, 5000 - fade_time, animate.linear)
        .then(bind(app.clockPowerImgView[i], function(){
          this.removeFromSuperview();
        }));
      }

      for(var i = 0; i < app.obstacles.spawnCount; i++)
      {
      	if(app.obstacles.entities[i].vx  != 0)
        	app.obstacles.entities[i].previousVx = app.obstacles.entities[i].vx;
        app.obstacles.entities[i].vx = 0;
      }
      setTimeout(function() {
        app._sound.stop("clock");
        app.isClockPowerTicking = false;
        for(var i = 0; i < app.obstacles.spawnCount; i++)
        {
          app.obstacles.entities[i].vx = app.obstacles.entities[i].previousVx;
        }
      }, 5000);
    }else if(power.id == "godmode"){
      app._sound.play("super");
      this.playerEntityPool.entities[0].view.setImage("resources/images/god_circle.png");
      this.playerEntityPool.isGodMode = true;
      this.playerEntityPool._trail.setImage("resources/images/god_trail.png");
      this.playerEntityPool._trail.updateOpts({height: 320});
      setTimeout(function() {
        app._sound.stop("super");
        app.playerEntityPool.entities[0].view.setImage("resources/images/circle.png");
        app.playerEntityPool.isGodMode = false;
      }, 5000);
      // this.playerEntityPool._trail.setImage("resources/images/god_trail.png");
      // this.playerEntityPool._trail.updateOpts({height: 320});
    }
  }

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});

/**
 * Player Class
 */
var Player = Class(Entity, function() {
  var sup = Entity.prototype;
  this.name = "Player";
});


var PlayerEntityPool = Class(EntityPool, function() {
  var sup = EntityPool.prototype;
  this.name = "playerEntityPool";

  var _player;
  var _playerMoveDown;
  var _tickCount;
  var _readyToMove;
  var _playerEntityPool;

  this.init = function(opts) {
    opts.ctor = Entity;
    sup.init.call(this, opts);
  };

  // This Pool is special, we don't destroy Player when reset the game
  // But we create them :)
  this.reset = function() {
    sup.reset.call(this);
    _playerEntityPool = this;
    _player = this.obtain(config.player);
    _tickCount = -1;
    _readyToMove = true;
    this.isGodMode = false;
    this._time = 0;
    SHOW_HIT_BOUNDS && _player.view.showHitBounds();
  };

  this.update = function(dt) {
    sup.update.call(this, dt);

    this._time += dt;

    if(this._trail){
      if(_playerMoveDown)
      {
        if(this.isGodMode)
          this._trail.updateOpts({y: _player.y - 300});
        else
          this._trail.updateOpts({y: _player.y - 100});
      }
      else
        this._trail.updateOpts({y: _player.y + 50});
    }

    //circle zooming when not move
    if(this.isGodMode)
    {
      _player.view.updateOpts({scale: 1, r: this._time / 100});
    }
    else if((_player.y == 70 || _player.y == BG_HEIGHT - 130) && _tickCount != -1)
    {
      this._time = 0;
      _tickCount += 0.005;
      _tickCount = _tickCount > config.CIRCLE_MAX_SCALE ? config.CIRCLE_MAX_SCALE : _tickCount;
      _player.view.updateOpts({scale: _tickCount});
      _player.model.radius = config.player.hitOpts.radius * _tickCount;
      this._trail.setImage("resources/images/trail.png");
      this._trail.updateOpts({height: 120});
    };


    // hungnt circle explode when reach CIRCLE_MAX_SCALE
    if(_tickCount == config.CIRCLE_MAX_SCALE)
    {
      //destroy old obstacles
      animate(_playerEntityPool.entities[0]).clear();

      for(var i = 0; i < app.obstacles.spawnCount; i++)
      {
        app.obstacles.entities[0].shade1.removeFromSuperview();
        app.obstacles.entities[0].shade2.removeFromSuperview();
        app.obstacles.entities[0].destroy();
      }
      app.obstacles.spawnCount = 0;

      _tickCount = -1;
      effects.firework(_player, {
        duration: 3000,
        scale: 0.9,
        images:
        [
          'resources/images/red_explode.png',
          'resources/images/blue_explode.png',
          'resources/images/green_explode.png',
          'resources/images/orange_explode.png',
          'resources/images/pink_explode.png',
          'resources/images/purple_explode.png',
          'resources/images/white_explode.png',
          'resources/images/yellow_explode.png'
        ]
      });
      _player.destroy();
      this._trail.removeFromSuperview();
      app._sound.play("break");
      app._sound.stop("background");
      app.isGameOver = true;
      app.showEndScreen();
    }
  }

  this.onInputSelect = function() {
    console.log("hungnt Player onInputSelect");
    if(app.isGameOver == false){
      if(_player.y == 70)
        this.playerMove(true); // Cicrle move down
      else if(_player.y == BG_HEIGHT - 130)
        this.playerMove(false); // Cicrle move up
    }
  };

  this.playerMove = function(isMoveDown){
    if(_readyToMove == false)
      return;

    app._sound.play("touch");

    _playerMoveDown = isMoveDown;

    this._trail = new ImageView({
      parent: app.view,
      width: 50,
      height: 120,
      layout: 'box',
      centerX: true,
      y: _player.y,
      zIndex: 2,
      image: 'resources/images/trail.png'
    });
    if(this.isGodMode){
      this._trail.setImage("resources/images/god_trail.png");
      this._trail.updateOpts({height: 320});
    }


    if(isMoveDown) this._trail.updateOpts({flipY: true});

    _readyToMove = false;
    animate(_player).now({y: isMoveDown ? BG_HEIGHT - 130 : 70}, 600, animate.linear)
    .then(this.resizePlayer()); // rescale player and add score

    var nextColor = choice(config.border_color);
    app.fade(app.borderView._subviews, app.borderColor, nextColor, 1000);
    app.borderColor = nextColor;
  };

  this.resizePlayer = function(){
    setTimeout(function() {
      if(app.isGameOver == false)
      {
        if(_playerMoveDown)
          animate(_player).now({scale: 1, centerX: true, y : BG_HEIGHT - 130}, 100, animate.linear);
        else
          animate(_player).now({scale: 1, centerX: true, y : 70}, 100, animate.linear);

        _player.model.radius = config.player.hitOpts.radius;
        _tickCount = 1;

        //remove trail
        if(_playerEntityPool._trail)
          _playerEntityPool._trail.removeFromSuperview();

        //add score
        app.score++;
        app.scoreTextView.setText("" + app.score);
        _readyToMove = true;
      }
    }, 700);
  };
});

/**
 * Obstacle Class
 * ~ an individual Obstacle
 */
var Obstacle = Class(Entity, function() {
  var sup = Entity.prototype;
  this.name = "Obstacle";

  this.update = function(dt) {
    if((this.view.x > BG_WIDTH && this.isRightObstacle == false) || (this.view.x < 0 /*&& this.vx < 0*/ && this.isRightObstacle == true))
    {
      app._sound.play("break");
      app.obstacles.spawnCount--;
      this.destroy();
      this.shade1.removeFromSuperview();
      this.shade2.removeFromSuperview();

      if(this.view._opts.image.indexOf("red_square") > -1)
      {
        effects.firework(this.view, {
          duration: 3000,
          images: ['resources/images/red_explode.png']
        });
      }else if(this.view._opts.image.indexOf("yellow_rectangle") > -1){
        effects.firework(this.view, {
          duration: 3000,
          images: ['resources/images/yellow_explode.png']
        });
      }else if(this.view._opts.image.indexOf("purple_rectangle") > -1){
        effects.firework(this.view, {
          duration: 3000,
          images: ['resources/images/purple_explode.png']
        });
      }else if(this.view._opts.image.indexOf("triangle") > -1){
        effects.firework(this.view, {
          duration: 3000,
          images: ['resources/images/green_explode.png']
        });
      }else if(this.view._opts.image.indexOf("polygon") > -1){
        effects.firework(this.view, {
          duration: 3000,
          images: ['resources/images/pink_explode.png']
        });
      }else if(this.view._opts.image.indexOf("star") > -1){
        effects.firework(this.view, {
          duration: 3000,
          images: ['resources/images/yellow_explode.png']
        });
      }
    }

    sup.update.call(this, dt);
  };
});

/**
 * Obstacles Class
 * ~ a collection of Obstacles
 */
var Obstacles = Class(EntityPool, function() {
  var sup = EntityPool.prototype;

  this.init = function(opts) {
    opts.ctor = Obstacle;
    sup.init.call(this, opts);
  };

  this.reset = function() {
    this.spawnCount = 0;
    this.spawnMin = 0;
    this.spawnMax = 0;
    sup.reset.call(this);
  };

  this.update = function(dt) {
    var current_range;

    if(app.score >= 40)
    {
      current_range = 6;
    }else if(app.score >= 30)
    {
      current_range = 5;
    }else if(app.score >= 25)
    {
      this.spawnMin = 1;
      this.spawnMax = 3;
      current_range = 4;
    }else if(app.score >= 20)
    {
      this.spawnMin = 2;
      this.spawnMax = 2;
      current_range = 3;
    }else if(app.score >= 10)
    {
      this.spawnMin = 1;
      this.spawnMax = 2;
      current_range = 2;
    }else if(app.score >= 0)
    {
      this.spawnMin = 1;
      this.spawnMax = 2;
      current_range = 1;
    }

    if(this.spawnCount < this.spawnMin)
    {
      var n = util.random(this.spawnMin, this.spawnMax + 1);
      if(current_range > config.obstacles.types.length) current_range = config.obstacles.types.length;
      for(var i = 0; i < n; i++)
      {
        var type = util.random(0, current_range);
        this.spawnObstacle(type);
        this.spawnCount++;
      }
    }

    //update square's shade
    for(var i = 0; i < this.entities.length; i++)
    {
      this.entities[i].shade1.style.x = this.entities[i].isRightObstacle == true ? this.entities[i].view.x + 15 : this.entities[i].view.x - 15
      this.entities[i].shade2.style.x = this.entities[i].isRightObstacle == true ? this.entities[i].shade1.style.x + 15 : this.entities[i].shade1.style.x - 15;
    }

    sup.update.call(this, dt);
  };

  this.spawnObstacle = function(val) {
    var type = config.obstacles.types[val];
    type.x = Math.random() > 0.5 ? rollFloat(-500, -200) : rollFloat(BG_WIDTH + 200, BG_WIDTH + 500); // random left/right obstacle

    //random Obstacle x and y
    if(type.x > 0) //obstacle from right side -> make vx < 0 to make it move left
      type.vx = -config.obstacles.speed ;
    else
      type.vx = config.obstacles.speed ;
    type.y = rollFloat(300, BG_HEIGHT - 300);


    //show alert view
    var alert = new ImageView({
      parent: app.view,
      x: 0,
      y: type.y,
      width: 288,
      height: 50,
      opacity: 0.5,
      zIndex: 3,
      image: "resources/images/red_square_alert.png"
    });

    if(type.id == "red_square")
    {
      alert.setImage("resources/images/red_square_alert.png");
      alert.updateOpts({height: 50});
    }else if(type.id == "yellow_rectangle"){
      alert.setImage("resources/images/yellow_rectangle_alert.png");
      alert.updateOpts({height: 50});
    }else if(type.id == "purple_rectangle"){
      alert.setImage("resources/images/purple_rectangle_alert.png");
      alert.updateOpts({height: 50});
    }else if(type.id == "triangle"){
      alert.setImage("resources/images/triangle_alert.png");
      alert.updateOpts({height: 100});
    }else if(type.id == "polygon"){
      alert.setImage("resources/images/polygon_alert.png");
      alert.updateOpts({height: 100});
    }else if(type.id == "star"){
      alert.setImage("resources/images/star_alert.png");
      alert.updateOpts({height: 100});
    }

    if(type.x > 0)
      alert.updateOpts({flipX: true});

    alert.style.x = type.x > 0 ? BG_WIDTH - 288 : 0;

    //hide after 1.5s, hardcode a bit T.T
    animate(alert).now({opacity: 0}, 1500, animate.linear).then(bind(this, function(){
      alert.removeFromSuperview();
    }));
    //end show alert

    var obstacle = this.obtain(type);
    if(type.x > 0)
      obstacle.isRightObstacle = true;
    else
      obstacle.isRightObstacle = false;

    //create Obstacle's shade
    obstacle.shade1 = new ImageView({
      parent: app.view,
      width: 60,
      height: 60,
      layout: 'box',
      opacity: 0.7,
      y: obstacle.view.y,
      zIndex: 4,
      image: 'resources/images/red_square.png'
    });
    obstacle.shade1.style.x = type.x > 0 ? obstacle.view.x + 15 : obstacle.view.x - 15;


    obstacle.shade2 = new ImageView({
      parent: app.view,
      width: 60,
      height: 60,
      layout: 'box',
      opacity: 0.3,
      y: obstacle.view.y,
      zIndex: 3,
      image: 'resources/images/red_square.png'
    });
    obstacle.shade2.style.x = type.x > 0 ? obstacle.view.x + 15 : obstacle.view.x - 15;

    if(type.id == "red_square")
    {
      obstacle.shade1.setImage("resources/images/red_square.png");
      obstacle.shade2.setImage("resources/images/red_square.png");
    }else if(type.id == "yellow_rectangle"){
      obstacle.shade1.setImage("resources/images/yellow_rectangle.png");
      obstacle.shade2.setImage("resources/images/yellow_rectangle.png");
      obstacle.shade1.updateOpts({width: 120});
      obstacle.shade2.updateOpts({width: 120});
    }else if(type.id == "purple_rectangle"){
      obstacle.shade1.setImage("resources/images/purple_rectangle.png");
      obstacle.shade2.setImage("resources/images/purple_rectangle.png");
      obstacle.shade1.updateOpts({width: 240});
      obstacle.shade2.updateOpts({width: 240});
    }else if(type.id == "purple_rectangle"){
      obstacle.shade1.setImage("resources/images/purple_rectangle.png");
      obstacle.shade2.setImage("resources/images/purple_rectangle.png");
      obstacle.shade1.updateOpts({width: 240});
      obstacle.shade2.updateOpts({width: 240});
    }else if(type.id == "triangle"){
      obstacle.shade1.setImage("resources/images/triangle.png");
      obstacle.shade2.setImage("resources/images/triangle.png");
      obstacle.shade1.updateOpts({width: 100, height: 100});
      obstacle.shade2.updateOpts({width: 100, height: 100});
    }else if(type.id == "polygon"){
      obstacle.shade1.setImage("resources/images/polygon.png");
      obstacle.shade2.setImage("resources/images/polygon.png");
      obstacle.shade1.updateOpts({width: 100, height: 100});
      obstacle.shade2.updateOpts({width: 100, height: 100});
    }
    else if(type.id == "star"){
      obstacle.shade1.setImage("resources/images/star.png");
      obstacle.shade2.setImage("resources/images/star.png");
      obstacle.shade1.updateOpts({width: 100, height: 100});
      obstacle.shade2.updateOpts({width: 100, height: 100});
    }

    SHOW_HIT_BOUNDS && obstacle.view.showHitBounds();
  };
});

/**
 * Power Class
 * ~ an individual Power
 */
var Power = Class(Entity, function() {
  var sup = Entity.prototype;
  this.name = "Power";

  this.update = function(dt) {
    if((this.view.x > BG_WIDTH && this.isRightPower == false) || (this.view.x < 0  && this.isRightPower == true))
    {
      app.powers.spawnCount--;
      this.destroy();
    }

    sup.update.call(this, dt);
  };
});

/**
 * Powers Class
 * ~ a collection of Powers
 */
var Powers = Class(EntityPool, function() {
  var sup = EntityPool.prototype;
  var _power;

  this.init = function(opts) {
    opts.ctor = Power;
    sup.init.call(this, opts);
  };

  this.reset = function() {
    this.spawnCount = 0;
    _power = this;
    sup.reset.call(this);
  };

  this.update = function(dt) {
    if(this.spawnCount < 1 && app.score > 10) //add power when score > 10
    {
      this.spawnCount++;
      setTimeout(function () {
        _power.spawnPowers();
      }, Math.floor(Math.random() * 10000));
    }
    sup.update.call(this, dt);
  };

  this.spawnPowers = function() {
    var type = choice(config.powers.types);
    // var type = config.powers.types[2];
    type.x = Math.random() > 0.5 ? rollFloat(-500, -200) : rollFloat(BG_WIDTH + 200, BG_WIDTH + 500); // random left/right power

    //random power x and y
    if(type.x > 0) //power from right side -> make vx < 0 to make it move left
    {
      if(type.id == "addScore")
        type.viewOpts.image = "resources/images/addScore_right.png";
      type.vx = -config.powers.speed;
    }
    else
      type.vx = config.powers.speed;

    type.y = rollFloat(300, BG_HEIGHT - 300);

    var power = this.obtain(type);
    if(type.x > 0){
      if(type.id != "addScore")
        power.view.updateOpts({flipX: true});
      power.isRightPower = true;
    }else
    {
      power.view.updateOpts({flipX: false});
      power.isRightPower = false;
    }

    power.id = type.id;

    SHOW_HIT_BOUNDS && power.view.showHitBounds();
  };
});