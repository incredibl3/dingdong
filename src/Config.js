import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;
var PLAYER_SIZE = 60;
var OBSTACLE_SPEED = 0.35;
var POWER_SPEED = 0.15;

exports = {
  CIRCLE_MAX_SCALE: 2,
  BG_CELL: 5,
  border_color: [
  	{r: 253, g: 201, b: 0},//yellow
  	{r: 188, g: 52, b: 70},//red
  	{r: 189, g: 48, b: 247},//purple
  	{r: 247, g: 59, b: 164},//pink
  	{r: 253, g: 146, b: 4},//orange
  	{r: 130, g: 198, b: 40},//green
  	{r: 48, g: 205, b: 253},//blue
  	{r: 255, g: 255, b: 255},//white
  ],
  BorderView: {
  	name: "BorderView",
  	x: 0,
  	y: 0,
  	width: BG_WIDTH,
  	height: BG_HEIGHT,
  	zIndex: 2,
  	children: [
		//top border
  		{
  			cls: "ui.View",
  			width: BG_WIDTH,
  			height: 10,
  			x: 0,
  			y: 0,
  			backgroundColor: "rgb(188, 52, 70)"
  		},
  		//bottom border
  		{
  			cls: "ui.View",
  			width: BG_WIDTH,
  			height: 10,
  			x: 0,
  			y: BG_HEIGHT - 10,
  			backgroundColor: "rgb(188, 52, 70)"
  		},
  		//left border
  		{
  			cls: "ui.View",
  			width: 10,
  			height: BG_HEIGHT,
  			x: 0,
  			y: 0,
  			backgroundColor: "rgb(188, 52, 70)"
  		},
  		//right border
  		{
  			cls: "ui.View",
  			width: 10,
  			height: BG_HEIGHT,
  			x: BG_WIDTH - 10,
  			y: 0,
  			backgroundColor: "rgb(188, 52, 70)"
  		},
  		//top dock
  		{
  			cls: "ui.View",
  			width: 40,
  			height: 90,
  			x: (BG_WIDTH - 40) / 2,
  			y: 10,
  			backgroundColor: "rgb(188, 52, 70)"
  		},
  		//bottom dock
  		{
  			cls: "ui.View",
  			width: 40,
  			height: 90,
  			x: (BG_WIDTH - 40) / 2,
  			y: BG_HEIGHT - 90,
  			backgroundColor: "rgb(188, 52, 70)"
  		},
  	]
  },
  obstacles: {
  	speed: OBSTACLE_SPEED,
  	types: [
  		{
  			id: "red_square",
  			x: -60,
  			y: 60,
  			vx: OBSTACLE_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 60,
  				height: 60,
  			},
			viewOpts: {
				width: 60,
				height: 60,
				zIndex: 5,
				image: "resources/images/red_square.png"
			},
  		},
  		{
  			id: "yellow_rectangle",
  			x: -120,
  			y: 60,
  			vx: OBSTACLE_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 120,
  				height: 60,
  			},
			viewOpts: {
				width: 120,
				height: 60,
				zIndex: 5,
				image: "resources/images/yellow_rectangle.png"
			},
  		},
  		{
  			id: "purple_rectangle",
  			x: -120,
  			y: 60,
  			vx: OBSTACLE_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 240,
  				height: 60,
  			},
			viewOpts: {
				width: 240,
				height: 60,
				zIndex: 5,
				image: "resources/images/purple_rectangle.png"
			},
  		},
  		{
  			id: "triangle",
  			x: -120,
  			y: 60,
  			vx: OBSTACLE_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 100,
  				height: 100,
  			},
			viewOpts: {
				width: 	100,
				height: 100,
				zIndex: 5,
				image: "resources/images/triangle.png"
			},
  		},
  		{
  			id: "polygon",
  			x: -120,
  			y: 60,
  			vx: OBSTACLE_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 100,
  				height: 100,
  			},
			viewOpts: {
				width: 100,
				height: 100,
				zIndex: 5,
				image: "resources/images/polygon.png"
			},
  		},
  		{
  			id: "star",
  			x: -120,
  			y: 60,
  			vx: OBSTACLE_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 100,
  				height: 100,
  			},
			viewOpts: {
				width: 100,
  				height: 100,
				zIndex: 5,
				image: "resources/images/star.png"
			},
  		}
  	]
  },
  powers: {
  	speed: POWER_SPEED,
  	types: [
  		{
  			id: "bomb",
  			x: -60,
  			y: 60,
  			vx: POWER_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 90,
  				height: 60,
  			},
			viewOpts: {
				width: 90,
				height: 60,
				zIndex: 5,
				image: "resources/images/bomb.png"
			},
  		},
  		{
  			id: "clock",
  			x: -120,
  			y: 60,
  			vx: POWER_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 90,
  				height: 60,
  			},
			viewOpts: {
				width: 90,
				height: 60,
				zIndex: 5,
				image: "resources/images/clock.png"
			},
  		},
		{
  			id: "godmode",
  			x: -120,
  			y: 60,
  			vx: POWER_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 90,
  				height: 60,
  			},
			viewOpts: {
				width: 90,
				height: 60,
				zIndex: 5,
				image: "resources/images/godmode.png"
			},
  		},
  		{
  			id: "addScore",
  			x: -120,
  			y: 60,
  			vx: POWER_SPEED,
  			vy: 0,
  			hitOpts: {
  				width: 90,
  				height: 60,
  			},
			viewOpts: {
				width: 90,
				height: 60,
				zIndex: 5,
				image: "resources/images/addScore.png"
			},
  		},
  	]
  },
  player: {
  	x: (BG_WIDTH - PLAYER_SIZE)/ 2,
  	y: 70,
	hitOpts: {
		offsetX: PLAYER_SIZE / 2,
		offsetY: PLAYER_SIZE / 2,
		radius: 30
	},
	viewOpts: {
		width: PLAYER_SIZE,
		height: PLAYER_SIZE,
		r: 0,
		zIndex: 3,
		image: "resources/images/circle.png"
	}
  }


};